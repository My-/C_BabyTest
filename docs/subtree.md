# How to merge one git project in to another

One way to add `C_BabyTest` to project is by using [git `subtree`][1] command). Using this method all this project with all commits will be added to your project making a best referencing you could do.
```bash
git subtree add -P <prefix> <repo> <rev>
```
For example: you have your `my-c-project` and you want to add this `C_BabyTest` project to it with all its commits. You could (I assume both projects uses git):
```bash
# first clone `C_BabyTest` in any location on you machine (but not in to `my-c-project`)
git clone https://gitlab.com/My-/C_BabyTest.git

# then go to your `my-c-project` directory and
git subtree add -P baby-test /path/to/C_BabyTest/ master
```
**Note:**
- `baby-test` - directory will be crated inside your `my-c-project` were all code of `C_BabyTest` will be placed there.
- `master` - is `C_BabyTest` branch you copying to your `my-c-project`.



[1]: https://github.com/git/git/blob/master/contrib/subtree/git-subtree.txt
