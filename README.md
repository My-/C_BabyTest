# C BabyTest
### This is a "baby" (simple) test library for C language. Testing for beginners.


Usually, then I code I write test just to be sure code works as expected. C is not nicest (easy to understand) language to work with, especially when you start to learn to code. Because of time pressure on projects I had I didn't want invest time to check available C Testing libraries. So I created my own simple "C BabyTest" library.
### With "C BabyTest" you can:
- `Test.assertTrue(bool actual, char * message):bool` : Test if `actual` is really `true`.
- `Test.assertFalse(bool actual, char * message):bool` : Test if `actual` is `false`.
- `Test.assertEqual_String(char * expected, char * actual, char * message):bool` : Test if `actual` string are the same as `expected` string.
- `Test.assertEqual_Int(int expected, int actual, char * message):bool` : Test if `actual` integer are the same as `expected` integer.
- `Test.assertEqual_Uint(__uint64_t expected, __uint64_t actual, char * message):bool` : Test if `actual` unsigned integer are the same as `expected` one.
- `Test.assertEqual_Double(double expected, double actual, double delta, char * message):bool` : Test if `actual` double are the same as `expected` one with a given `delta` value (allowed difference).

Note each test function returns true if a test is passed.

<!-- ### Example:
Is always good practice to separate test code from main programs code. But for this example for simplicity reasons is not the case

```C
#include <stdio.h>
#include <stdbool.h>    // to suport booleans
#include "test.h"       // this library
// Note: this include shoud point to location where library is.
// In my case it is in the same folder.

#define MY_AGE 30 // constant to hold my age

// function you want to test
bool imOlder(int yourAge){
    // some creazy logic you not sure about
    return MY_AGE < yourAge;
}

int bobAge = 20;    // Bob age

// you know what you older then Bob.
bool shouldBe_True = imOlder(bobAge);

// test if function implementation is correct
Test.assertTrue(shouldBe_True, "I'm older then Bob");

```
To run on Windows + VisualStudio hit run.
To run (on Linx):
```bash
# go to yours tests files folder
# compile all ".c" files to "myTest.out" file
gcc ./*.c -o ./myTest.out

# if no compile errors run it
./myTest.out
```
So then you run it result is:

> <p><span style="color:white">I'm older then Bob: </span> <span style="color:yellow"> true </span> - <span style="color:red"> FAIL </span><p>
So you know what your implementation is wrong. Lets fix it. Change function to:
```C
bool imOlder(int yourAge){
    // some creazy logic you not sure about
    return MY_AGE > yourAge; // changed "<" to ">"
}
```
Then you run result now:
> <p><span style="color:white">I'm older then Bob: </span> <span style="color:yellow"> true </span> - <span style="color:green"> PASS </span><p>

Now you know you know what function works as expected.
 -->

<!-- - ![#f03c15](https://placehold.it/15/f03c15/000000?text=+) `#f03c15`
- ![#c5f015](https://placehold.it/15/c5f015/000000?text=+) `#c5f015`
- ![#14f02a](https://placehold.it/150x15/14f02a/000000?text=+) `#14f02a` -->


### How To use:

If you can I would recommend to use git `subtree` command to add this project to yours. This way I'll get credit as my commits will be in your project after doing it. I created [short tutorial](./docs/subtree.md) how to do it.  
Lets assume your `some-c-project` structure is as bellow:

```bash
.
├── baby_test       # folder containing C BabyTest code
│   ├── Readme.MD
│   └── src
│       ├── color.h
│       ├── test.c
│       └── test.h  
│   
├── src             # your programs source code
│   ├── program.c
│   └── program.h
│
└── test            # tests code
    ├── test_program.c
    └── test_program.h
```

Lets say `program.h` is like:
```c
#ifndef SOME_C_PROGRAM
#define SOME_C_PROGRAM

// add two integers
int add(int a, int b);
// divide two numbers
double divide(double a, double b);
// add two unsigned integers
uint64_t addBig(uint64_t a, uint64_t b);
// concat two strings
char* concat(const char* s1, const char* s2);
#endif
```

Lets say `program.c` is like:
```c
#include <stdio.h>
#include "program.h"

int add(int a, int b){
    return a + b;
}

double divide(double a, double b){
    return a / b;
}

uint64_t addBig(uint64_t a, uint64_t b){
    return a + b;
}

char* concat(const char *s1, const char *s2){
    // ref: https://stackoverflow.com/a/8465083/5322506
    char* result = malloc(strlen(s1) + strlen(s2) + 1); // +1 for the null-terminator
    // in real code you would check for errors in malloc here
    strcpy(result, s1);
    strcat(result, s2);
    return result;
}
```

Test should look like (`test_program.h`):
```c
#ifndef TEST_PROGRAM
#define TEST_PROGRAM

#include "../baby_test/src/test.h"
#include "../src/program.h"

void test_add();
void test_addBig();
void test_divide();
void test_concat();

void run_all();
#endif
```
and (`test_program.c`):
```c
#include "test_program.h"

void test_add(){
    Test.assertEqual_Int(5, add(2, 3), "2 + 3");
}

void test_addBig(){
    Test.assertEqual_Uint(5000000, addBig(2000000, 3000000), "2000000 + 3000000");
}

void test_divide(){
    Test.assertEqual_Double(0.66, divide(2, 3, 0.001), "2 / 3 with delta 0.001");
}

void test_concat(){
    Test.assertEqual_Int("hi c", concat("hi ", "c"), "hi and c");
}

void run_all(){
    test_add();
    test_addBig();
    test_divide();
    test_concat();
}
```

----

Find issue? possible improvement? Please, create an issue or PR.

[1]: ./docs/subtree.md
