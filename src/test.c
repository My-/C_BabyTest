/**
*   @by Mindaugas Sharskus
*   @date 2/04/2018
*/

#include "test.h"

// it's like a class.
struct test Test = {
    .display = test_display,
    .assertTrue = test_assertTrue,
    .assertFalse = test_assertFalse,
    .assertEqual_String = test_assertEqual_String,
    .assertEqual_Int = test_assertEqual_Int,
    .assertEqual_Uint = test_assertEqual_Uint,
    .assertEqual_Double = test_assertEqual_Double
};

void test_display(char* text, char* color){
    printf("%s%s\n" COLOR_RESET, color, text);
}

bool test_assertTrue(bool actual, char * message){
    printf("%s: %s - ", message, (YELLOW_TEXT "true" COLOR_RESET));
    if( actual ){ printf("%s\n", PASS); }
    else{ printf("%s\n", FAIL); }

    return actual;
}

bool test_assertFalse(bool actual, char * message){
    printf("%s: %s - ", message, (YELLOW_TEXT "false" COLOR_RESET));
    if( !actual ){ printf("%s\n", PASS); }
    else{ printf("%s\n", FAIL); }

    return !actual;
}

bool test_assertEqual_String(char* expected, char* actual, char* message){
    bool R = true;
    printf("%s: %s%s%s - ", message, YELLOW_TEXT, expected, COLOR_RESET);
    int compare = strcmp(expected, actual);
    if( compare == 0 ){ printf("%s\n", PASS); }
    else{
        printf("%s\n", FAIL);
        printf("\tExpected: %s\n", expected);
        printf("\tBut was : %s\n", actual);
        R = false;
    }

    return R;
}

bool test_assertEqual_Int(int expected, int actual, char* message){
    bool R = true;
    printf("%s: %s%d%s - ", message, YELLOW_TEXT, expected, COLOR_RESET);
    int compare = expected -actual;
    if( compare == 0 ){ printf("%s\n", PASS); }
    else{
        printf("%s\n", FAIL);
        printf("\tExpected: %d\n", expected);
        printf("\tBut was : %d\n", actual);
        R = false;
    }

    return R;
}

bool test_assertEqual_Uint(__uint64_t expected, __uint64_t actual, char * message){
    bool R = true;
    printf("%s: %s%lu%s - ", message, YELLOW_TEXT, expected, COLOR_RESET);
    bool not_same = expected ^ actual;
    if( not_same ){ printf("%s\n", PASS); }
    else{
        printf("%s\n", FAIL);
        printf("\tExpected: %lu\n", expected);
        printf("\tBut was : %lu\n", actual);
        R = false;
    }

    return R;
}

bool test_assertEqual_Double(double expected, double actual, double delta, char * message){
    bool R = true;
    printf("%s: %s%f%s - ", message, YELLOW_TEXT, expected, COLOR_RESET);
    double difference = expected - actual;
    difference *= difference < 0 ? -1 : 1;
    if(difference < delta ){ printf("%s\n", PASS); }
    else{
        printf("%s\n", FAIL);
        printf("\tExpected: %f (delta: %f)\n", expected, delta);
        printf("\tBut was : %f\n", actual);
        R = false;
    }

    return R;
}
